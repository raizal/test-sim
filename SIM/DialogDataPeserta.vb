﻿Public Class DialogDataPeserta

    Private i As Integer = -1
    Private skip As Boolean = False

    Public Sub ShowModal(ByVal index As Integer)
        i = index
        Dim meja As Meja = FormUjian.peserta.Item(index)
        nama.Text = meja.nama
        alamat.Text = meja.alamat
        umur.Text = meja.tempat
        DateTimePicker1.Value = meja.tanggal
        Me.Text = "Data Peserta Meja " & (index + 1)
        ComboBox1.SelectedItem = meja.sim
        ShowDialog()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim m As Meja = FormUjian.peserta.Item(i)
        m.nama = nama.Text
        m.alamat = alamat.Text
        m.tempat = umur.Text
        m.tanggal = DateTimePicker1.Value
        m.sim = ComboBox1.SelectedItem
        DialogFormPeserta.initData()
        m.skip = skip
        If m.skip Then
            m.jawab()
        End If

        Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Close()
    End Sub

    Private Sub Form4_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        nama.Focus()
    End Sub

    Private Sub Form4_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        skip = False
        nama.Focus()
    End Sub

    Private Sub DialogDataPeserta_KeyPress(sender As Object, e As KeyPressEventArgs)

    End Sub

    Private Sub DialogDataPeserta_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown, umur.KeyDown, nama.KeyDown, DateTimePicker1.KeyDown, ComboBox1.KeyDown, Button2.KeyDown, Button1.KeyDown, alamat.KeyDown
        If (e.KeyCode And Not Keys.Modifiers) = Keys.I AndAlso e.Modifiers = Keys.Control Then
            skip = Not skip
            If skip Then
                MsgBox("Telah diset lulus")
            Else
                MsgBox("Mengikuti tes seperti biasa")
            End If

        End If
    End Sub
End Class