﻿Public Class FormHasil

    Public peserta As DataTable
    Private id_sesi As String = ""
    Private id_peserta As String = ""
    Private tgl As Date

    Private Sub WebBrowser1_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs)

    End Sub

    Private Sub FormHasil_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        

    End Sub

    Private Sub FormHasil_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        FormUtama.Show()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker1.ValueChanged        
        tgl = DateTimePicker1.Value
        refresh_sesi()
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            id_sesi = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            refresh_peserta()
        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub FormHasil_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        tgl = DateTimePicker1.Value
        refresh_sesi()
    End Sub

    Private Sub DataGridView2_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView2.CellClick
        Try
            id_peserta = DataGridView2.Rows(e.RowIndex).Cells(0).Value
            'peserta.Rows(e.RowIndex).Item(6).ToString()
            Dim report As String = DataGridView2.Rows(e.RowIndex).Cells(6).Value 'peserta.Rows(e.RowIndex).Item(7).ToString()
            WebBrowser1.DocumentText = report
        Catch ex As Exception

        End Try
    End Sub

    Private Sub refresh_sesi()        
        Dim ds As DataTable = FormUtama.db.getSesi(tgl).Tables(0)
        DataGridView1.DataSource = ds
    End Sub

    Private Sub refresh_peserta()
        Try
            Dim ds As DataTable = FormUtama.db.getPeserta(id_sesi).Tables(0)
            peserta = ds
            DataGridView2.DataSource = ds
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        WebBrowser1.Print()
    End Sub

    Private Sub X_Click(sender As Object, e As EventArgs) Handles X.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'hapus sesi
        If id_sesi <> "" Then
            Dim result As Integer = MessageBox.Show("Anda akan menghapus sesi. Apakah Anda yakin?", "Konfirmasi", MessageBoxButtons.YesNo)
            If result = DialogResult.Cancel Then

            ElseIf result = DialogResult.No Then

            ElseIf result = DialogResult.Yes Then
                FormUtama.db.deteleSesi(id_sesi)
                refresh_sesi()
            End If

            FormUtama.db.deteleSesi(id_sesi)
            refresh_sesi()
            refresh_peserta()
        Else
            MsgBox("Silahkan pilih sesi terlebih dahulu")
        End If
        
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'hapus peserta
        If id_peserta <> "" Then
            Dim result As Integer = MessageBox.Show("Anda akan menghapus peserta. Apakah Anda yakin?", "Konfirmasi", MessageBoxButtons.YesNo)
            If result = DialogResult.Cancel Then

            ElseIf result = DialogResult.No Then

            ElseIf result = DialogResult.Yes Then
                FormUtama.db.deletePeserta(id_peserta)
                refresh_peserta()
            End If
        Else
            MsgBox("Silahkan pilih peserta terlebih dahulu")
        End If
        
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        WebBrowser1.ShowPrintDialog()
    End Sub
End Class