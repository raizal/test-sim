﻿Public Class DialogSettingPort

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        FormUtama.SerialPort1.PortName = ComboBox1.Text
        Try
            FormUtama.SerialPort1.Open()
            My.Settings.port = ComboBox1.Text
            My.Settings.lulus = NumericUpDown1.Value
            My.Settings.Save()
            Me.Close()

            FormUtama.SerialPort1.Close()
        Catch ex As Exception
            MsgBox("Tidak dapat terhubung dengan perangkat!")
        End Try
        
    End Sub

    Private Sub Form5_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        ComboBox1.Items.Clear()
        ' Show all available COM ports.
        Dim i As Integer = 0
        For Each sp As String In My.Computer.Ports.SerialPortNames
            ComboBox1.Items.Add(sp)
            If sp.Equals(My.Settings.port) Then
                ComboBox1.SelectedItem = sp
            End If
            i = i + 1
        Next
        NumericUpDown1.Value = My.Settings.lulus

    End Sub

    Private Sub Form5_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ComboBox1.Items.Clear()
        ' Show all available COM ports.
        Dim i As Integer = 0
        For Each sp As String In My.Computer.Ports.SerialPortNames
            ComboBox1.Items.Add(sp)
            If i = 0 Then
                ComboBox1.SelectedItem = sp
            End If

            If sp.Equals(My.Settings.port) Then
                ComboBox1.SelectedItem = sp
            End If
            i = i + 1
        Next

        My.Settings.port = ComboBox1.SelectedItem
        My.Settings.Save()

    End Sub
End Class