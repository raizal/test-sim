﻿Public Class FormUtama

    Public db As New DB

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        FormUjian.initData()
        DialogFormPeserta.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        DialogSettingPort.ShowDialog()
    End Sub

    Sub tryConnect(ByVal index As Integer)

        Try
            If index < My.Computer.Ports.SerialPortNames.Count Then
                SerialPort1.PortName = My.Computer.Ports.SerialPortNames(index)
                SerialPort1.Open()
                My.Settings.port = SerialPort1.PortName
                SerialPort1.Close()
                My.Settings.Save()
            Else
                MsgBox("Perangkat tidak terdeteksi")
            End If

        Catch ex As Exception
            index = index + 1
            tryConnect(index)
        End Try
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        FormHasil.Show()
    End Sub

    Private Sub FormUtama_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Try
            SerialPort1.PortName = My.Settings.port
            SerialPort1.Open()
            SerialPort1.Close()
        Catch ex As Exception
            tryConnect(0)
        End Try

    End Sub

    Private Sub FormUtama_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Escape Then
            If SerialPort1.IsOpen Then
                SerialPort1.Close()

            End If
            End
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        FormPenguji.ShowDialog()
    End Sub
End Class
