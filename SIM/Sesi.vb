﻿Public Class Sesi
    Public peserta As New List(Of Meja)    
    Public tanggal As String
    Public penguji As String

    Public awal As System.DateTime
    Public akhir As System.DateTime

    Sub putPeserta(ByVal valPeserta As List(Of Meja))
        For Each p As Meja In valPeserta
            If Not p.nama.Equals("") Then
                peserta.Add(p)
            End If
        Next        
    End Sub

    Sub New()
        Dim regDate As Date = Date.Now()
        awal = System.DateTime.Now()
        tanggal = regDate.ToString("dd-MM-yyyy")
    End Sub
End Class
