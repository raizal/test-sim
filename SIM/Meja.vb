﻿Public Class Meja
    Public nama As String
    Public tempat As String
    Public alamat As String
    Public jawaban As New List(Of Short)
    Public report As String
    Public benar As Integer = 0
    Public tanggal As Date
    Public skip As Boolean = False
    Public sim As String = "C"

    Sub New(ByVal varnama As String, varTempat As String, lahir As Date, varalamat As String)
        Me.nama = varnama
        Me.tempat = varTempat
        Me.alamat = varalamat
        tanggal = lahir
        report = IO.Path.GetTempFileName()
        report = report.Replace(".tmp", ".html")

        For i As Integer = 0 To 29
            jawaban.Add(0)
        Next

    End Sub

    Public Sub jawab()
        Dim b As Integer = New System.Random().Next(21, 30)
        Dim salah As Integer = 30 - b

        For i As Integer = 0 To 29

            If FormUjian.daftarSoal(FormUjian.indexSoal(i) - 1).jawaban Then
                jawaban(i) = 2
            Else
                jawaban(i) = 1
            End If

            If salah > 0 And (i + 1) Mod 2 = 0 Then
                If jawaban(i) = 1 Then
                    jawaban(i) = 2
                Else
                    jawaban(i) = 1
                End If
                salah = salah - 1
            End If

        Next

    End Sub

    Public Sub SetJawaban(ByVal index As Integer, jawaban As Integer)
        If skip = False Then
            If jawaban > 0 Then
                Me.jawaban.Item(index) = jawaban
            End If
        End If
    End Sub

    Public Sub clear()
        nama = ""
        alamat = ""
        tempat = ""
        benar = 0
        For i As Integer = 0 To jawaban.Count - 1
            jawaban(i) = 0
        Next
    End Sub

End Class
