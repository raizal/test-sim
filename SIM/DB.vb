﻿Imports System.Data.OleDb

Public Class DB

    Public con As OleDbConnection
    Sub New()
        con = New OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=db.accdb")
        If con.State = ConnectionState.Closed Then
            con.Open()
        End If
    End Sub

    Sub insertPenguji(ByVal data As String)
        Dim sql As String = "insert into penguji(nama) values(?)"
        Dim cmd As New OleDb.OleDbCommand(sql, con)        
        cmd.Parameters.AddWithValue("@p1", data)
        cmd.ExecuteNonQuery()
    End Sub

    Sub updatePenguji(ByVal data As String, id As String)
        Dim sql As String = "update penguji set nama=? where id=" & id
        Dim cmd As New OleDb.OleDbCommand(sql, con)
        cmd.Parameters.AddWithValue("@p1", data)
        cmd.ExecuteNonQuery()
    End Sub

    Sub insertSesi(ByVal data As Sesi)

        Dim sql As String = "insert into sesi(waktu_awal,waktu_akhir) values(?,?)"
        Dim cmd As New OleDb.OleDbCommand(sql, con)
        cmd.Parameters.AddWithValue("@p1", data.awal.ToString("dd\/MM\/yyyy hh.mm.ss"))
        cmd.Parameters.AddWithValue("@p2", data.akhir.ToString("dd\/MM\/yyyy hh.mm.ss"))
        cmd.ExecuteNonQuery()

        sql = "select * from sesi where waktu_awal=? and waktu_akhir=?"
        cmd = New OleDbCommand(sql, con)
        cmd.Parameters.AddWithValue("@p1", data.awal.ToString("dd\/MM\/yyyy hh.mm.ss"))
        cmd.Parameters.AddWithValue("@p2", data.akhir.ToString("dd\/MM\/yyyy hh.mm.ss"))
        Dim dr As OleDbDataReader = cmd.ExecuteReader

        dr.Read()
        Dim id As String = dr("id").ToString()

        For Each p As Meja In data.peserta
            sql = "insert into peserta(nama,alamat,tempat_lahir,tanggal_lahir,sim,id_sesi,report) values(?,?,?,?,?,?,?)"
            cmd = New OleDb.OleDbCommand(sql, con)

            cmd.Parameters.AddWithValue("@p1", p.nama)
            cmd.Parameters.AddWithValue("@p2", p.alamat)
            cmd.Parameters.AddWithValue("@p3", p.tempat)
            cmd.Parameters.AddWithValue("@p4", p.tanggal.Date.ToString("dd\/MM\/yyyy"))
            cmd.Parameters.AddWithValue("@p5", p.sim)
            cmd.Parameters.AddWithValue("@p6", id)
            cmd.Parameters.AddWithValue("@p7", New IO.StreamReader(p.report).ReadToEnd())
            cmd.ExecuteNonQuery()
        Next

    End Sub

    Function getSesi(ByVal par As Date) As DataSet
        Dim myString As String = "select * from sesi where DateValue(waktu_awal) = #" & par.ToString("MM\/dd\/yyyy") & "#"
        Dim myCommand As OleDb.OleDbCommand = New OleDb.OleDbCommand(myString, con)
        Dim myAdapter As New OleDb.OleDbDataAdapter
        'myCommand.Parameters.Add("@DATE", OleDb.OleDbType.Date).Value = par
        myAdapter.SelectCommand = myCommand
        Dim ds As New DataSet()
        myAdapter.Fill(ds)
        Return ds
    End Function

    Function getPeserta(ByVal par As String) As DataSet
        Dim myString As String = "select id_peserta,nama,alamat,tempat_lahir,tanggal_lahir,sim,report from peserta where id_sesi = @p"
        Dim myCommand As OleDb.OleDbCommand = New OleDb.OleDbCommand(myString, con)
        Dim myAdapter As New OleDb.OleDbDataAdapter
        myCommand.Parameters.AddWithValue("@p", par)
        myAdapter.SelectCommand = myCommand
        Dim ds As New DataSet()
        myAdapter.Fill(ds)
        Return ds
    End Function

    Function getPenguji() As DataSet
        Dim myString As String = "select nama,id from penguji"
        Dim myCommand As OleDb.OleDbCommand = New OleDb.OleDbCommand(myString, con)
        Dim myAdapter As New OleDb.OleDbDataAdapter
        myAdapter.SelectCommand = myCommand
        Dim ds As New DataSet()
        myAdapter.Fill(ds)
        Return ds
    End Function

    Public Sub deteleSesi(ByVal id As String)
        Dim myString As String = "delete from peserta where id_sesi=@p"
        Dim sql As New OleDbCommand(myString, con)
        sql.Parameters.AddWithValue("@p", id)
        sql.ExecuteNonQuery()

        Dim q As String = "delete from sesi where id=@p"
        Dim sql2 As New OleDbCommand(q, con)
        sql2.Parameters.AddWithValue("@p", id)
        sql2.ExecuteNonQuery()
    End Sub

    Public Sub deletePeserta(ByVal id As String)
        Dim myString As String = "delete from peserta where id_peserta=@p"
        Dim sql As New OleDbCommand(myString, con)
        sql.Parameters.AddWithValue("@p", id)
        sql.ExecuteNonQuery()
    End Sub

    Public Sub deletePenguji(ByVal id As String)
        Dim myString As String = "delete from penguji where id=@p"
        Dim sql As New OleDbCommand(myString, con)
        sql.Parameters.AddWithValue("@p", id)
        sql.ExecuteNonQuery()
    End Sub


End Class
