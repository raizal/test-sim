﻿Public Class FormPenguji

    Dim id_penguji As String = ""

    Sub refreshData()

        DataGridView1.DataSource = FormUtama.db.getPenguji().Tables(0)
        TextBox1.Clear()
        id_penguji = ""
        DialogFormPeserta.refreshPenguji()
    End Sub


    Private Sub FormPenguji_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        refreshData()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'simpan
        If Not id_penguji.Equals("") Then
            FormUtama.db.updatePenguji(TextBox1.Text, id_penguji)
            refreshData()
        End If

    End Sub

    Private Sub FormPenguji_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        refreshData()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        FormUtama.db.insertPenguji(TextBox1.Text)
        refreshData()

    End Sub


    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs)

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)
        
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Not id_penguji.Equals("") Then
            FormUtama.db.deletePenguji(id_penguji)
            refreshData()
        End If
    End Sub

    Private Sub DataGridView1_CellClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Try
            TextBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            id_penguji = DataGridView1.Rows(e.RowIndex).Cells(1).Value
        Catch ex As Exception
            id_penguji = ""
        End Try
    End Sub
End Class