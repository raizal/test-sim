﻿Imports System.IO

Public Class FormUjian
    Const startPage As Integer = 0

    Dim currentPage As Integer = startPage

    Dim readTime As Integer
    Dim answerTime As Integer
    Dim endTime As Integer
    Dim tmppath As String

    Public peserta As New List(Of Meja)
    Public daftarSoal As New List(Of Soal)

    Public indexSoal As New List(Of Integer)
    Dim nilai_bulat As Integer
    Public penguji As String

    Dim sesiUjian As Sesi

    Public Sub initData()
        readTime = 0
        answerTime = 0
        endTime = 0
        currentPage = startPage
        tmppath = System.IO.Path.GetTempFileName()
        tmppath = tmppath.Replace(".tmp", ".avi")

        peserta.Clear()
        indexSoal.Clear()
        For i As Integer = 1 To 30
            peserta.Add(New Meja("", "", Date.Now(), ""))
            indexSoal.Add(i)
        Next


        Shuffle(indexSoal)
        'soal
        'Petunjuk penambahan soal
        'nama file => soalNoUrut , masukkan ke resource
        'kalo image, extensi harus .png , kalo video .avi
        daftarSoal.Add(New Soal("Golongan SIM-B diperlukan jika hendak mengemudikan sepeda motor 150 cc", False)) '1
        daftarSoal.Add(New Soal("Menurut pasal 81 (2,True)) UU No. 22 tahun 2009 umur minimal untuk dapat memiliki SIM golongan C adalah 16 tahun", False)) '2 
        daftarSoal.Add(New Soal("Menurut pasal 107 (2,True)) UU No. 22 Tahun 2009 pengemudi sepeda motor diwajibkan untuk menyalakan lampu pada siang hari", True)) '3 
        daftarSoal.Add(New Soal("Bagi seorang pengemudi yang terlibat sebagai tersangka dalam kasus kecelakaan lalu lintas dengan akibat korban luka berat atau meninggal dunia atau si pengemudi terbukti beberapa kali melakukan pelanggaran lalu lintas, sesuai pasal 89 (2,True)) UU No. 2009 Polri berwenang untuk melakukan uji ulang tentang penerbitan SIM", False)) '4
        daftarSoal.Add(New Soal("image", True)) '5
        daftarSoal.Add(New Soal("video", False)) '6
        daftarSoal.Add(New Soal("video", False)) '7
        daftarSoal.Add(New Soal("video", True)) '8 soal geje
        daftarSoal.Add(New Soal("image", False)) '9
        daftarSoal.Add(New Soal("image", False)) '10
        daftarSoal.Add(New Soal("image", False)) '11
        daftarSoal.Add(New Soal("image", False)) '12
        daftarSoal.Add(New Soal("image", False)) '13
        daftarSoal.Add(New Soal("image", False)) '14
        daftarSoal.Add(New Soal("image", False)) '15
        daftarSoal.Add(New Soal("image", True)) '16
        daftarSoal.Add(New Soal("video", False)) '17
        daftarSoal.Add(New Soal("image", False)) '18
        daftarSoal.Add(New Soal("image", False)) '19
        daftarSoal.Add(New Soal("image", True)) '20
        daftarSoal.Add(New Soal("image", True)) '21
        daftarSoal.Add(New Soal("image", False)) '22
        daftarSoal.Add(New Soal("image", False)) '23
        daftarSoal.Add(New Soal("image", False)) '24
        daftarSoal.Add(New Soal("image", True)) '25
        daftarSoal.Add(New Soal("image", False)) '26
        daftarSoal.Add(New Soal("image", True)) '27
        daftarSoal.Add(New Soal("video", True)) '28
        daftarSoal.Add(New Soal("video", True)) '29
        daftarSoal.Add(New Soal("image", False)) '30

        For I As Integer = 1 To 30
            If peserta.Item(I - 1).nama.Equals("") Then
                CType(Me.Controls("m" & I), PictureBox).Image = led.Images(0)
            Else
                CType(Me.Controls("m" & I), PictureBox).Image = led.Images(1)
            End If
        Next

    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private answeringSection As Boolean = False

    Private Sub Timer_Tick(sender As Object, e As EventArgs) Handles Timer.Tick

        If Not serial.IsOpen Then
            TimerCOMError.Enabled = True
        End If

        If readTime > 0 Then
            jawaban.Visible = False
            readTime = readTime - 1
            time.Text = "00:" + readTime.ToString("D2")
            answeringSection = True
        ElseIf readTime <= 0 Then

            If answerTime > 0 Then
                jawaban.Visible = False
                answerTime = answerTime - 1
                time.Text = "00:" + answerTime.ToString("D2")
                answeringSection = True
                My.Computer.Audio.Play(My.Resources.ResourceManager.GetObject("beep"), AudioPlayMode.WaitToComplete)
            ElseIf endTime > 0 Then
                jawaban.Visible = True
                answeringSection = False
                endTime = endTime - 1
                Dim jawabanSoal As String
                If (daftarSoal.Item(indexSoal.Item(currentPage - 1) - 1).jawaban) = True Then
                    jawabanSoal = "BENAR"
                Else
                    jawabanSoal = "SALAH"
                End If
                jawaban.Text = "Jawaban untuk pertanyaan ini adalah " & jawabanSoal
                time.Text = "00:" + endTime.ToString("D2")
            ElseIf endTime <= 0 Then
                jawaban.Visible = False
                readTime = 60
                answerTime = 6
                endTime = 5
                'next pertanyaan
                Try
                    serial.WriteLine("a")
                Catch ex As Exception

                End Try

                For I As Integer = 1 To 30
                    If peserta.Item(I - 1).nama.Equals("") Then
                        CType(Me.Controls("m" & I), PictureBox).Image = led.Images(0)
                    Else
                        CType(Me.Controls("m" & I), PictureBox).Image = led.Images(1)
                    End If
                Next

                currentPage = currentPage + 1
                If currentPage > 30 Then
                    'soal terakhir
                    answeringSection = False
                    Timer.Enabled = False
                    report()
                    Me.sesiUjian.putPeserta(peserta)
                    sesiUjian.akhir = System.DateTime.Now()
                    FormUtama.db.insertSesi(sesiUjian)
                    answeringSection = False
                    Try
                        serial.Close()
                    Catch ex As Exception

                    End Try

                    Me.Close()
                Else
                    'nomor soal
                    Dim index As Integer = indexSoal.Item(currentPage - 1)
                    Dim thread As New Threading.Thread(
                        Sub()
                            My.Computer.Audio.Play(My.Resources.ResourceManager.GetObject("suara" & currentPage), AudioPlayMode.WaitToComplete)
                            My.Computer.Audio.Play(My.Resources.ResourceManager.GetObject("Recording_C" & index.ToString("D2")), AudioPlayMode.WaitToComplete)
                        End Sub
                    )
                    thread.Start()

                    soal.Visible = False
                    soalGambar.Visible = False
                    soalVideo.Visible = False

                    Dim soalGet As String = daftarSoal.Item(index - 1).soal

                    If soalGet.Equals("video") Then

                        soalVideo.Visible = True
                        If System.IO.File.Exists(tmppath) = True Then
                            System.IO.File.Delete(tmppath)
                        End If
                        soalVideo.currentPlaylist.clear()
                        Dim tmp As System.IO.Stream = System.IO.File.Create(tmppath)
                        tmp.Write(My.Resources.ResourceManager.GetObject("soal" & index), 0, My.Resources.ResourceManager.GetObject("soal" & index).Length)
                        tmp.Close()

                        soalVideo.uiMode = "none"
                        soalVideo.currentMedia = soalVideo.newMedia(tmppath)
                        soalVideo.settings.setMode("loop", True)
                        soalVideo.Ctlcontrols.play()

                    ElseIf soalGet.Equals("image") Then
                        soalGambar.Visible = True
                        soalGambar.Image = CType(My.Resources.ResourceManager.GetObject("soal" & index), System.Drawing.Image)
                    Else
                        soal.Visible = True
                        soal.Text = soalGet
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub Form2_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            My.Computer.Audio.Stop()
            If System.IO.File.Exists(tmppath) = True Then
                System.IO.File.Delete(tmppath)
            End If

            If System.IO.File.Exists(tmppath) = True Then
                System.IO.File.Delete(tmppath)
            End If
        Catch ex As Exception

        End Try
        FormUtama.Show()
    End Sub

    'generate report
    Private Sub report()
        For no As Integer = 0 To 29
            If peserta.Item(no).nama.Equals("") Then
                'skip kalo belum di isi namanya
            Else
                Dim r As String = My.Resources.report
                Dim p As Meja = peserta.Item(no)
                r = Replace(r, "<%%nama>", p.nama)
                r = Replace(r, "<%%tempat>", p.tempat)
                r = Replace(r, "<%%tanggal>", Date.Now().ToString("dd-MM-yyyy"))
                r = Replace(r, "<%%lahir>", p.tanggal.ToString("dd-MM-yyyy"))
                r = Replace(r, "<%%alamat>", p.alamat)
                r = Replace(r, "<%%sim>", p.sim)
                r = Replace(r, "<%%penguji>", penguji)
                Dim jawabanPeserta As List(Of Short) = p.jawaban

                For i As Integer = 0 To 29
                    Dim tanda As String = "(&#x2718;)"

                    If daftarSoal.Item(indexSoal.Item(i) - 1).jawaban = True And jawabanPeserta(i) = 2 Then
                        p.benar = p.benar + 1
                        tanda = "(&#x2714;)"
                    ElseIf daftarSoal.Item(indexSoal.Item(i) - 1).jawaban = False And jawabanPeserta(i) = 1 Then
                        p.benar = p.benar + 1
                        tanda = "(&#x2714;)"
                    End If

                    If jawabanPeserta(i) = 1 Then
                        r = Replace(r, "<%%soal" & (i + 1) & ">", "S " & tanda)
                    ElseIf jawabanPeserta(i) = 2 Then
                        r = Replace(r, "<%%soal" & (i + 1) & ">", "B " & tanda)
                    Else
                        r = Replace(r, "<%%soal" & (i + 1) & ">", "-")
                    End If

                Next
                'NILAI
                'minimal 70 = 21 yg benar
                If p.benar >= 21 Then
                    r = Replace(r, "<%%lulus>", "LULUS")
                Else
                    r = Replace(r, "<%%lulus>", "TIDAK LULUS")
                End If
                nilai_bulat = p.benar * 100 / 30
                'r = Replace(r, "<%%nilai>", (p.benar * 100 / 30).ToString())
                r = Replace(r, "<%%nilai>", (nilai_bulat).ToString())
                System.IO.File.WriteAllText(p.report, r)

            End If
        Next
    End Sub

    Sub Shuffle(Of T)(list As IList(Of T))
        Dim r As Random = New Random()
        For i = 0 To list.Count - 1
            Dim index As Integer = r.Next(i, list.Count)
            If i <> index Then
                ' swap list(i) and list(index)
                Dim temp As T = list(i)
                list(i) = list(index)
                list(index) = temp
            End If
        Next
    End Sub

    Private Sub Label30_Click(sender As Object, e As EventArgs) Handles Label30.Click

    End Sub

    Private Sub m16_Click(sender As Object, e As EventArgs) Handles m16.Click

    End Sub

    Private Sub Label28_Click(sender As Object, e As EventArgs) Handles Label28.Click

    End Sub

    Private Sub m17_Click(sender As Object, e As EventArgs) Handles m17.Click

    End Sub

    Private Sub Label27_Click(sender As Object, e As EventArgs) Handles Label27.Click

    End Sub

    Private Sub m24_Click(sender As Object, e As EventArgs) Handles m24.Click

    End Sub

    Private Sub Label25_Click(sender As Object, e As EventArgs) Handles Label25.Click

    End Sub

    Private Sub m25_Click(sender As Object, e As EventArgs) Handles m25.Click

    End Sub

    Private Sub Label26_Click(sender As Object, e As EventArgs) Handles Label26.Click

    End Sub

    Private Sub m18_Click(sender As Object, e As EventArgs) Handles m18.Click

    End Sub

    Private Sub Label24_Click(sender As Object, e As EventArgs) Handles Label24.Click

    End Sub

    Private Sub m19_Click(sender As Object, e As EventArgs) Handles m19.Click

    End Sub

    Private Sub Label23_Click(sender As Object, e As EventArgs) Handles Label23.Click

    End Sub

    Private Sub m26_Click(sender As Object, e As EventArgs) Handles m26.Click

    End Sub

    Private Sub Label21_Click(sender As Object, e As EventArgs) Handles Label21.Click

    End Sub

    Private Sub m27_Click(sender As Object, e As EventArgs) Handles m27.Click

    End Sub

    Private Sub Label22_Click(sender As Object, e As EventArgs) Handles Label22.Click

    End Sub

    Private Sub m20_Click(sender As Object, e As EventArgs) Handles m20.Click

    End Sub

    Private Sub Label19_Click(sender As Object, e As EventArgs) Handles Label19.Click

    End Sub

    Private Sub m28_Click(sender As Object, e As EventArgs) Handles m28.Click

    End Sub

    Private Sub Label20_Click(sender As Object, e As EventArgs) Handles Label20.Click

    End Sub

    Private Sub m21_Click(sender As Object, e As EventArgs) Handles m21.Click

    End Sub

    Private Sub Label18_Click(sender As Object, e As EventArgs) Handles Label18.Click

    End Sub

    Private Sub m22_Click(sender As Object, e As EventArgs) Handles m22.Click

    End Sub

    Private Sub Label17_Click(sender As Object, e As EventArgs) Handles Label17.Click

    End Sub

    Private Sub m29_Click(sender As Object, e As EventArgs) Handles m29.Click

    End Sub

    Private Sub m30_Click(sender As Object, e As EventArgs) Handles m30.Click

    End Sub

    Private Sub Label15_Click(sender As Object, e As EventArgs) Handles Label15.Click

    End Sub

    Private Sub serial_DataReceived(sender As Object, e As System.IO.Ports.SerialDataReceivedEventArgs) Handles serial.DataReceived
        If answeringSection = True Then
            Try
                Dim received As String = ""
                If serial.IsOpen() Then

                    received = serial.ReadLine()

                End If


                If received.First().Equals(";") Then
                    received = received.Substring(1, received.Length - 1)
                    If received.Substring(received.Length - 1, 1).Equals(";") Then
                        received = received.Substring(0, received.Length - 1)
                    End If
                End If

                Dim data As Array = received.Split(";")
                If data.Length > 0 And currentPage < 31 Then
                    For i As Integer = 0 To 29

                        If Trim(data(i)).Equals("") Then
                            data(i) = "0"
                        End If

                        peserta.Item(i).SetJawaban(currentPage - 1, data(i))
                        If peserta.Item(i).nama.Equals("") Then
                            CType(Me.Controls("m" & i + 1), PictureBox).Image = led.Images(0)
                        Else
                            If data(i).Equals("0") Then
                                CType(Me.Controls("m" & i + 1), PictureBox).Image = led.Images(1)
                            Else
                                CType(Me.Controls("m" & i + 1), PictureBox).Image = led.Images(2)
                            End If
                        End If
                    Next
                End If

            Catch ex As Exception

            End Try

        End If
    End Sub

    Private Sub WebBrowser1_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles WebBrowser1.DocumentCompleted
        CType(sender, WebBrowser).Print()
    End Sub

    Private Sub Label31_Click(sender As Object, e As EventArgs) Handles Label31.Click
        Close()
    End Sub

    Private Sub serial_ErrorReceived(sender As Object, e As Ports.SerialErrorReceivedEventArgs) Handles serial.ErrorReceived

    End Sub

    Public Sub start()
        Try
            If serial.IsOpen Then
                serial.Close()
            End If
            serial.PortName = My.Settings.port
            serial.Open()
            DialogFormPeserta.Close()
            sesiUjian = New Sesi()
            Timer.Enabled = True
            Me.Show()
        Catch ex As Exception
            Me.Hide()
            MsgBox("Tidak dapat terhubung dengan perangkat! Silahkan cek pengaturan Anda!")
        End Try
    End Sub

    Private Sub FormUjian_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

    End Sub

    Private Sub TimerCOMError_Tick(sender As Object, e As EventArgs) Handles TimerCOMError.Tick
        If Not serial.IsOpen Then
            Timer.Enabled = False

            Try
                serial.Close()
            Catch ex As Exception

            End Try

            Try
                serial.Open()
                Timer.Enabled = True
            Catch ex As Exception

            End Try
        Else
            TimerCOMError.Enabled = False
        End If

    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click

    End Sub
End Class
