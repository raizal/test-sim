﻿Public Class DialogFormPeserta
    'ubah   
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button98.Click, Button84.Click, Button70.Click, Button56.Click, Button448.Click, Button434.Click, Button420.Click, Button42.Click, Button406.Click, Button392.Click, Button378.Click, Button364.Click, Button336.Click, Button322.Click, Button308.Click, Button294.Click, Button280.Click, Button28.Click, Button266.Click, Button252.Click, Button224.Click, Button210.Click, Button196.Click, Button182.Click, Button168.Click, Button154.Click, Button140.Click, Button126.Click, Button112.Click, Button1.Click
        Dim gb As GroupBox = CType(CType(sender, Button).Parent, GroupBox)
        Dim index As Integer = Convert.ToInt32(gb.Text.Replace("Meja ", ""))
        DialogDataPeserta.ShowModal(index - 1)
    End Sub

    'kosongkan
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button97.Click, Button83.Click, Button69.Click, Button55.Click, Button447.Click, Button433.Click, Button419.Click, Button41.Click, Button405.Click, Button391.Click, Button377.Click, Button363.Click, Button335.Click, Button321.Click, Button307.Click, Button293.Click, Button279.Click, Button27.Click, Button265.Click, Button251.Click, Button223.Click, Button209.Click, Button2.Click, Button195.Click, Button181.Click, Button167.Click, Button153.Click, Button139.Click, Button125.Click, Button111.Click
        Dim gb As GroupBox = CType(CType(sender, Button).Parent, GroupBox)
        Dim index As Integer = Convert.ToInt32(gb.Text.Replace("Meja ", ""))
        Dim label As Label = CType(gb.Controls("nama" & index), Label)
        label.Text = "Nama Peserta : -"
        Dim m As Meja = FormUjian.peserta.Item(index - 1)
        m.clear()
    End Sub

    Public Sub initData()
        For Each m As Control In Me.Controls
            If m.GetType() Is GetType(GroupBox) Then
                Dim gb As GroupBox = CType(m, GroupBox)
                Dim index As Integer = Convert.ToInt32(gb.Text.Replace("Meja ", ""))
                Dim label As Label = CType(gb.Controls("nama" & index), Label)
                label.Text = "Nama Peserta : -"
                Try
                    If Not FormUjian.peserta.Item(index - 1).nama.Equals("") Then
                        label.Text = "Nama Peserta : " & FormUjian.peserta.Item(index - 1).nama
                    End If
                Catch ex As Exception

                End Try                
            End If
        Next
        refreshPenguji()
    End Sub

    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        initData()
    End Sub

    Private Sub Form3_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Hide()
        FormUtama.Show()
    End Sub

    Public Sub refreshPenguji()
        daftarPenguji.Items.Clear()
        For Each row In FormUtama.db.getPenguji().Tables(0).Rows
            daftarPenguji.Items.Add(row(0))
        Next

    End Sub

    Private Sub Button225_Click(sender As Object, e As EventArgs) Handles Button225.Click
        FormUjian.penguji = daftarPenguji.Text
        FormUjian.start()
    End Sub
End Class